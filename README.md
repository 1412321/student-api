# Student API Project Summary

* Server build with Rails (can't run in local without ruby and rails)
* Javacript client in root path (use AJAX Jquery to take data from API)
* This project was built in Ubuntu.
* Project may not run in Windows locally (Please test in deployed app)
* Database: **SQLite**
* Ruby version: **2.4.0**
* Repo address: https://gitlab.com/minhluong96/student-api

### Project main file:
* Database config: `/config/database.yml`
* API folder: `/app/controllers/api`
* Main controller: `/app/controllers/home_controller.rb`
* Routing: `/config/routes.rb`
* Client JS code (use jQuery): `/app/assets/javascripts/index.js`

### How to build:
* Install gem (version 2.4.0)
* Run `gem install bundler`
* `cd` to project folder
* Run `bundle install`
* Run `rails db:create && rails db:migrate && rails db:seed` (This step to initialize database)
* Run `rails s` (Run app)
* Test client app in http://localhost:3000
* Test api in http://localhost:3000/api/students

### Deploy address:
* Demo app deploy in: https://minhluong96-studentapi.herokuapp.com/

* API in (response with JSON file): 
All student: https://minhluong96-studentapi.herokuapp.com/api/students
One student: https://minhluong96-studentapi.herokuapp.com/api/students/1

