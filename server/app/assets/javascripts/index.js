$(document).ready(function(){
    loadDataToStudentTable();
});

function loadDataToStudentTable(){
    $.get('/api/students').done(function(data){
        $("#student").find("tr:gt(0)").remove();
        for(var i = 0; i < data.length; i++){
            $('#student tr:last').after(loadRow(data[i]))
        }
    })
}

function loadRow(data){
    return '<tr><td>'+ data.student_id + '</td><td>' +
           data.name + '</td><td>' + data.address +'</td><td>' +
           data.birthday + '</td><td class="text-right"><button class="btn btn-link" onclick="editStudent('+ data.id + ')">' +
           '<span class="glyphicon glyphicon-pencil"></span></button>' +
           '<button class="btn btn-link" onclick="deleteStudent('+ data.id + ')">' +
           '<span class="glyphicon glyphicon-remove"></span></button></td></tr>'
}

function deleteStudent(id){
  swal({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then(function () {
    $.ajax({
      url: '/api/students/' + id,
      type: 'delete'
    }).done(function(){
      loadDataToStudentTable();
      swal(
        'Deleted!',
        'Student has been deleted.',
        'success'
      )
    })
  })
}

function editStudent(id){
    $.get('/api/students/' + id).done(function(data){
    var birthday;
    if (data.birthday == null)
      birthday = '';
    else
      birthday = data.birthday;
    swal({
        title: 'Edit Student',
        html:
          '<input id="id" class="swal2-input" placeholder="Student ID" value="'+ data.student_id + '">' +
          '<input id="name" class="swal2-input" placeholder="Name" value="'+ data.name + '">' +
          '<input id="address" class="swal2-input" placeholder="Address" value="'+ data.address + '">' +
          '<input id="birthday" class="swal2-input" placeholder="Birthday (DD/MM/YYYY)" value="'+ birthday + '">' ,
        focusConfirm: false,
        preConfirm: function () {
          return new Promise(function (resolve) {
            resolve([
              id,
              $('#id').val(),
              $('#name').val(),
              $('#address').val(),
              $('#birthday').val()
            ])
          })
        }
      }).then(function (result) {
        putStudent(result);
      }).catch(swal.noop)
    })
}

function addStudent(){
    swal({
        title: 'Add Student',
        html:
          '<input id="id" class="swal2-input" placeholder="Student ID">' +
          '<input id="name" class="swal2-input" placeholder="Name">'+
          '<input id="address" class="swal2-input" placeholder="Address">'+
          '<input id="birthday" class="swal2-input" placeholder="Birthday (DD/MM/YYYY)">',
        focusConfirm: false,
        preConfirm: function () {
          return new Promise(function (resolve) {
            resolve([
              $('#id').val(),
              $('#name').val(),
              $('#address').val(),
              $('#birthday').val()
            ])
          })
        }
      }).then(function (result) {
        postStudent(result);
      }).catch(swal.noop)
}

function putStudent(result){
  $.ajax({
    url: "/api/students/" + result[0],
    data: {
        student: {
          student_id: result[1],
          name: result[2],
          address: result[3],
          birthday: result[4]
      }
    },
    type: 'PUT',
    dataType: 'JSON'
  }).done(function(result){
      loadDataToStudentTable();
  }).fail(function(error){
      console.log(error);
      swal('Error', error.responseJSON.errors[0], 'error');
  })
}

function postStudent(result){
    $.post("/api/students",
        {
          student: {
            student_id: result[0],
            name: result[1],
            address: result[2],
            birthday: result[3]
        }
    },function(result){
        loadDataToStudentTable();
    }, 'json').fail(function(error){
        console.log(error);
        swal('Error', error.responseJSON.errors[0], 'error');
    })
}