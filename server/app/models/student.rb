class Student < ApplicationRecord
    validates :student_id, presence: true
    validates :name, presence: true
    validates :address, presence: true
end
