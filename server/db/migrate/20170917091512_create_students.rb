class CreateStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :students do |t|
      t.string :name, null: false
      t.string :student_id, null: false, unique: true
      t.date :birthday
      t.string :address

      t.timestamps
    end
  end
end
